import java.util.ArrayList;
import java.util.Arrays;

public class Task {

    public void task6(Object object) {
        System.out.println(isArray(object));
    }

    public static boolean isArray(Object object) {
        return object != null && object.getClass().isArray();
    }

    public void task7(int n) {
        int[] nArray = { 1, 2, 3, 4, 5, 6 };
        for (var i = 0; i < nArray.length; i++) {
            var ele = nArray[i];
            if (i == n) {
                System.out.println(ele);
                return;
            }

        }
        System.out.println("null");
    }

    public void task8(int[] array) {
        Arrays.sort(array);
        System.out.println(Arrays.toString(array));
    }

    public void task9(int index) {
        int[] nArray = { 1, 2, 3, 4, 5, 6 };
        for (var i = 0; i < nArray.length; i++) {
            var ele = nArray[i];
            if (ele == index) {
                System.out.println("the index is:" + i);
                return;
            }

        }
        System.out.println("The index is: -1");

    }

}
