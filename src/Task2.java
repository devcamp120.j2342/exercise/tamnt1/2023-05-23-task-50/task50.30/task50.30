import java.util.ArrayList;
import java.util.List;

public class Task2 {

    public void task1(Object[] object) {
        List<Object> filteredList = new ArrayList();
        for (Object obj : object) {
            if (obj instanceof String || obj instanceof Number) {
                filteredList.add(obj);
            }
        }

        System.out.println(filteredList);

    }

    public void task2(int n) {
        int[] nArray = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        List<Integer> filterArray = new ArrayList<>();
        for (int el : nArray) {
            if (el != n) {
                filterArray.add(el);
            }

        }
        System.out.println(filterArray);
    }

    public void task3() {
        int[] randomArray = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        int randomNumber = (int) Math.floor(Math.random() * randomArray.length);
        System.out.println(randomNumber);
    }

    public void task4(int x, int y) {
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        while (arrayList.size() <= x) {
            arrayList.add(y);
        }

        System.out.println(arrayList);
    }

    public void task5(int x, int y) {
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        while (arrayList.size() < y) {
            arrayList.add(x++);
        }

        System.out.println(arrayList);
    }
}
