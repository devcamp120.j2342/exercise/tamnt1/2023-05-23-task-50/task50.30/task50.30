
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class App {
    public static void main(String[] args) throws Exception {
        subTask1(1);
        subTask2(3);
        subTask3("Robin Singh");
        subTask4("Robin Singh from USA");
        subTask5("JavaScript exercises");
        subTask6("js string exercises");
        subTask7(3);
        subTask8(4);
        subTask9();
        subTask10("345");
    }

    public static void task1(Object object) {
        System.out.println(isArray(object));
    }

    public static boolean isArray(Object object) {
        return object != null && object.getClass().isArray();
    }

    public static void task2(int n) {
        int[] nArray = { 1, 2, 3, 4, 5, 6 };
        for (var i = 0; i < nArray.length; i++) {
            var ele = nArray[i];
            if (i == n) {
                System.out.println(ele);
                return;
            }

        }
        System.out.println("null");
    }

    public static void task3(int[] array) {
        Arrays.sort(array);
        System.out.println(Arrays.toString(array));
    }

    public static void task4(int index) {
        int[] nArray = { 1, 2, 3, 4, 5, 6 };
        for (var i = 0; i < nArray.length; i++) {
            var ele = nArray[i];
            if (ele == index) {
                System.out.println("the index is:" + i);
                return;
            }

        }
        System.out.println("The index is: -1");

    }

    public static void task5(int[] array1, int[] array2) {
        int length1 = array1.length;
        int length2 = array2.length;
        int[] result = new int[length1 + length2];
        System.arraycopy(array1, 0, result, 0, length1);
        System.arraycopy(array2, 0, result, length1, length2);
        System.out.println(result);

    }

    public static void task6(Object[] object) {
        ArrayList<Object> filteredList = new ArrayList<Object>();
        for (Object obj : object) {
            if (obj instanceof String
                    || obj instanceof Number && !(obj instanceof Double && Double.isNaN((Double) obj))) {
                filteredList.add(obj);
            }
        }

        System.out.println("task 6:" + filteredList);

    }

    public static void task7(int n) {
        int[] nArray = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        List<Integer> filterArray = new ArrayList<>();
        for (int el : nArray) {
            if (el != n) {
                filterArray.add(el);
            }

        }
        System.out.println(filterArray);
    }

    public static void task8() {
        int[] randomArray = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        int randomNumber = (int) Math.floor(Math.random() * randomArray.length);
        System.out.println(randomNumber);
    }

    public static void task9(int x, int y) {
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        while (arrayList.size() <= x) {
            arrayList.add(y);
        }

        System.out.println(arrayList);
    }

    public static void task10(int x, int y) {
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        while (arrayList.size() < y) {
            arrayList.add(x++);
        }

        System.out.println(arrayList);
    }

    // 50.30

    public static void subTask1(Object input) {
        if (input instanceof String) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }

    }

    public static void subTask2(int n) {
        String str = "Robin Singh";
        String result = str.substring(0, n);
        System.out.println(result);

    }

    public static void subTask3(String string) {
        String[] result = string.split("");
        for (String element : result) {
            System.out.print(element + " ");
        }

    }

    public static void subTask4(String input) {
        String output = input.replaceAll(" ", "-").toLowerCase();
        System.out.println(output);

    }

    public static void subTask5(String input) {
        String[] words = input.split(" ");
        StringBuilder outputBuilder = new StringBuilder();

        for (String word : words) {
            String modifiedWord = word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase();
            outputBuilder.append(modifiedWord);
        }

        String output = outputBuilder.toString();
        System.out.println(output);

    }

    public static void subTask6(String input) {
        String[] words = input.split(" ");
        StringBuilder outputBuilder = new StringBuilder();

        for (String word : words) {
            String modifiedWord = word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase();
            outputBuilder.append(modifiedWord).append(" ");
        }
        String output = outputBuilder.toString().trim();
        System.out.println(output);

    }

    public static void subTask7(int n) {
        String word = "Ha!";

        StringBuilder resultBuilder = new StringBuilder();

        for (int i = 0; i < n; i++) {
            resultBuilder.append(word).append(" ");
        }

        String result = resultBuilder.toString().trim();
        System.out.println(result);

    }

    public static void subTask8(int n) {
        String input = "dcresource";
        int length = input.length();
        int numOfElements = (int) Math.ceil((double) length / n);
        String[] result = new String[numOfElements];

        for (int i = 0; i < numOfElements; i++) {
            int startIndex = i * n;
            int endIndex = Math.min(startIndex + n, length);
            result[i] = input.substring(startIndex, endIndex);
        }

        System.out.println(Arrays.toString(result));

    }

    public static void subTask9() {
        String input = "The quick brown fox jumps over the lazy dog";
        String substring = "the";
        int count = 0;
        int index = input.toLowerCase().indexOf(substring.toLowerCase());

        while (index != -1) {
            count++;
            index = input.toLowerCase().indexOf(substring.toLowerCase(), index + 1);
        }

        System.out.println(count);

    }

    public static void subTask10(String replacementString) {
        String originalString = "0000";
        int n = replacementString.length();
        String result = originalString.substring(0, originalString.length() - n) + replacementString;
        System.out.println(result);

    }
}
